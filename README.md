Name
This project is named MBook

Description
This project is a multi-tier app to manage books. It contains :
- An Angular web client
- A Spring Boot REST API with Unit Tests included
- An End-to-End test of the Angular client developed with Cypress
This project also aims to apply the DevOps principle to multi-tier apps

Installation
To use the app, we need to :
1. Create a MySQL database named "tp_devops"
2. Run the Spring Boot REST API
	- Open the spring boot project with your chosen IDE and run it from there (it is run on the port 9090)
3. Run the Angular Web client
	- Install the necessary dependencies with the command "npm install"
	- Run the web client with the command "ng serve --port 8081"
4. Run the End-to-End test
	- Install the necessary dependencies with the command "npm install cypress --save-dev"
	- Run the End-to-End test with the command "npx cypress open"